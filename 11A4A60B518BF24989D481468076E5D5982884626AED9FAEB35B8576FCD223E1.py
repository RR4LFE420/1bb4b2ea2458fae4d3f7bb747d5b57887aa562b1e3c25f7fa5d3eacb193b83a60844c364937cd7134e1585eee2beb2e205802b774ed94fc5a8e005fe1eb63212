#!/usr/bin/python
import threading;import time;import sys;import signal;import configparser;import audioop;import subprocess as sp;import argparse;import os.path;import pymumble.pymumble_py3 as pymumble;import hashlib;import youtube_dl;import logging;import base64;from PIL import Image;from io import BytesIO;import random;import wave;import asyncio;import threading;from multiprocessing import Process;from subprocess import call;import os ##muhlines
class player():
    def __init__(self):
        self.level = 1
        self.exp = 0
    def grind(self):
        self.exp = self.exp + 1

    def status(self):
        return 


class IffyAh(): 
    def __init__(self, ip, port, cert, key, name, gencert = False):
        if gencert == True:
            os.system("openssl req -subj '/CN=www.mydom.com/O=My Company Name LTD./C=US' -x509 -newkey rsa:2048 -nodes -keyout key.pem -out cert.pem")
        self.m = pymumble.Mumble(str(ip), port=int(port),certfile="cert.pem", keyfile="key.pem", user=str(name))
        self.m.set_application_string("Happy bot")
        self.m.start()
        self.m.is_ready()
        self.m.set_bandwidth(48000)      
    def move_into_most_users_channel(self):
     
        arr = []
        for u in self.m.users:
            arr.append(self.m.users[u].get_property("channel_id"))

        values = []
        for i, e in enumerate(arr): 
            values.append([])
            values[i].append(e)
            values[i].append(0)
            for j, el in enumerate(arr):
                if e == el:
                    values[i][1] += 1
        
        top = 0
        pick = 0
        for v in values: 
            if v[1] > top:
                top = v[1]
                pick = v[0]
        self.m.channels[pick].move_in()  

    def move_into_every_channel(self,delay=0.1):
        for c in self.m.channels:
            self.m.channels[c].move_in()
            time.sleep(delay)

    def move_into_random_channel(self):
        channels = []
        for c in self.m.channels:
            channels.append(c)
        self.m.channels[random.choice(channels)].move_in()
    def print_channel_names(self):
        for c in self.m.channels:
            print (" channel id: " + str(self.m.channels[c].get_property("channel_id")), end=" ")
            print (" channel name: "+ str(self.m.channels[c].get_property("name")), end=" ")      
            print (" description: " + str(self.m.channels[c].get_property("description")), end=" ")
            print()

    def print_user_names(self):
        print(self.m.users)
        for c in self.m.users:
            print (" name: " + str(self.m.users[c].get_property("name")), end="")
            print (" channel: " + str(self.m.users[c].get_property("channel_id")))

    def send_global_message(self,message):
        for c in self.m.users:
            time.sleep(0.1)
            self.m.users[c].send_message(message)

    def count_channels(self): 
        print ("server has: "+str(len(self.m.channels))+" channels")

    def count_users(self):
        print("server has: "+str(self.m.users.count())+" users")

    def play_file(self,sound_file):

        f = open(sound_file,"rb")

        #ffmpeg -i sound.wav -ac 1 -ar 48000 -f s16le -acodec pcm_s16le sound.pcm
        self.m.sound_output.set_audio_per_packet(0.02)
        chunk = ""
        ticks = 0
        while 1:
            while self.m.sound_output.get_buffer_size() > 0.5:
                time.sleep(0.01)
     
            chunk = f.read(2)
            if chunk:
                self.m.sound_output.add_sound(chunk)
            if ticks == 100000:
                ticks = 0
                #self.move_into_most_users_channel()
            if len(chunk) == 0: 
                break
            while self.m.sound_output.get_buffer_size() > 0.1:
                time.sleep(0.01)        
            ticks += 1
        f.close()


    def play_file_stick_on_users(self,sound_file):

        f = open(sound_file,"rb")

        #ffmpeg -i sound.wav -ac 1 -ar 48000 -f s16le -acodec pcm_s16le sound.pcm
        self.m.sound_output.set_audio_per_packet(0.02)
        chunk = ""
        ticks = 0
        while 1:
            while self.m.sound_output.get_buffer_size() > 0.5:
                time.sleep(0.01)
     
            chunk = f.read(2)
            if chunk:
                self.m.sound_output.add_sound(chunk)
            if ticks == 100000:
                ticks = 0
                self.move_into_most_users_channel()
            if len(chunk) == 0: 
                break
            while self.m.sound_output.get_buffer_size() > 0.1:
                time.sleep(0.01)        
            ticks += 1
        f.close()